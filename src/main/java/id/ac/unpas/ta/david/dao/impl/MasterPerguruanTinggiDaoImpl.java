package id.ac.unpas.ta.david.dao.impl;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import id.ac.unpas.ta.david.dao.MasterPerguruanTinggiDao;
import id.ac.unpas.ta.david.pojo.MasterPerguruanTinggi;



@Service("masterPerguruanTinggiDao")
@Transactional
@Repository
public class MasterPerguruanTinggiDaoImpl extends BaseDao<MasterPerguruanTinggi> implements MasterPerguruanTinggiDao {

	@Override
	public boolean saveMasterPerguruanTinggi(MasterPerguruanTinggi masterPerguruanTinggi) {
		// TODO Auto-generated method stub
		return save(masterPerguruanTinggi);
	}

	@Override
	public boolean updateMasterPerguruanTinggi(MasterPerguruanTinggi masterPerguruanTinggi) {
		// TODO Auto-generated method stub
		return update(masterPerguruanTinggi);
	}

	@Override
	public boolean deleteMasterPerguruanTinggi(MasterPerguruanTinggi masterPerguruanTinggi) {
		// TODO Auto-generated method stub
		return delete(masterPerguruanTinggi);
	}

	@Override
	public MasterPerguruanTinggi getMasterPerguruanTinggi(String kodePerguruanTinggi) {
		// TODO Auto-generated method stub
		Criteria criteria = createCriteria(MasterPerguruanTinggi.class);
		criteria.add(Restrictions.eq("kodePerguruanTinggi", kodePerguruanTinggi));
		return (MasterPerguruanTinggi) criteria.uniqueResult();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<MasterPerguruanTinggi> getListMasterPerguruanTinggi() {
		// TODO Auto-generated method stub
		Criteria criteria = createCriteria(MasterPerguruanTinggi.class);
		criteria.addOrder(Order.asc("kodePerguruanTinggi"));
		return criteria.list();
	}

}
