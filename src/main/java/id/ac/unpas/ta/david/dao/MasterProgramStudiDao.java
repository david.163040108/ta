package id.ac.unpas.ta.david.dao;

import java.util.List;

import id.ac.unpas.ta.david.pojo.MasterFakultas;
import id.ac.unpas.ta.david.pojo.MasterProgramStudi;



public interface MasterProgramStudiDao {
	public boolean saveMasterProgramStudi(MasterProgramStudi masterProgramStudi);
	public boolean updateMasterProgramStudi(MasterProgramStudi masterProgramStudi);
	public boolean deleteMasterProgramStudi(MasterProgramStudi masterProgramStudi);
	
	public MasterProgramStudi getMasterProgramStudi(String kodeProgramStudi);
	
	public List<MasterProgramStudi> getListMasterProgramStudi();
	public List<MasterProgramStudi> getListMasterProgramStudi(MasterFakultas masterFakultas);
}
