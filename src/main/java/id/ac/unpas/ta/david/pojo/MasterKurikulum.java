package id.ac.unpas.ta.david.pojo;
// Generated Jul 15, 2019 5:45:00 PM by Hibernate Tools 5.2.8.Final

import java.util.HashSet;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * MasterKurikulum generated by hbm2java
 */
@Entity
@Table(name = "master_kurikulum", catalog = "ta")
public class MasterKurikulum implements java.io.Serializable {

	private int idMasterKurikulum;
	private MasterProgramStudi masterProgramStudi;
	private int tahunKurikulum;
	private String namaKurikulum;
	private String inisialKurikulum;
	private int sksTa1;
	private int sksTa2;
	private int jumlahTa;
	private int aktif;
	private Set<MasterMahasiswa> masterMahasiswas = new HashSet<MasterMahasiswa>(0);
	private Set<MasterMataKuliah> masterMataKuliahs = new HashSet<MasterMataKuliah>(0);

	public MasterKurikulum() {
	}

	public MasterKurikulum(int idMasterKurikulum, MasterProgramStudi masterProgramStudi, int tahunKurikulum,
			String namaKurikulum, String inisialKurikulum, int sksTa1, int sksTa2, int jumlahTa, int aktif) {
		this.idMasterKurikulum = idMasterKurikulum;
		this.masterProgramStudi = masterProgramStudi;
		this.tahunKurikulum = tahunKurikulum;
		this.namaKurikulum = namaKurikulum;
		this.inisialKurikulum = inisialKurikulum;
		this.sksTa1 = sksTa1;
		this.sksTa2 = sksTa2;
		this.jumlahTa = jumlahTa;
		this.aktif = aktif;
	}

	public MasterKurikulum(int idMasterKurikulum, MasterProgramStudi masterProgramStudi, int tahunKurikulum,
			String namaKurikulum, String inisialKurikulum, int sksTa1, int sksTa2, int jumlahTa, int aktif,
			Set<MasterMahasiswa> masterMahasiswas, Set<MasterMataKuliah> masterMataKuliahs) {
		this.idMasterKurikulum = idMasterKurikulum;
		this.masterProgramStudi = masterProgramStudi;
		this.tahunKurikulum = tahunKurikulum;
		this.namaKurikulum = namaKurikulum;
		this.inisialKurikulum = inisialKurikulum;
		this.sksTa1 = sksTa1;
		this.sksTa2 = sksTa2;
		this.jumlahTa = jumlahTa;
		this.aktif = aktif;
		this.masterMahasiswas = masterMahasiswas;
		this.masterMataKuliahs = masterMataKuliahs;
	}

	@Id

	@Column(name = "idMasterKurikulum", unique = true, nullable = false)
	public int getIdMasterKurikulum() {
		return this.idMasterKurikulum;
	}

	public void setIdMasterKurikulum(int idMasterKurikulum) {
		this.idMasterKurikulum = idMasterKurikulum;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "idProgramStudi", nullable = false)
	public MasterProgramStudi getMasterProgramStudi() {
		return this.masterProgramStudi;
	}

	public void setMasterProgramStudi(MasterProgramStudi masterProgramStudi) {
		this.masterProgramStudi = masterProgramStudi;
	}

	@Column(name = "tahunKurikulum", nullable = false)
	public int getTahunKurikulum() {
		return this.tahunKurikulum;
	}

	public void setTahunKurikulum(int tahunKurikulum) {
		this.tahunKurikulum = tahunKurikulum;
	}

	@Column(name = "namaKurikulum", nullable = false)
	public String getNamaKurikulum() {
		return this.namaKurikulum;
	}

	public void setNamaKurikulum(String namaKurikulum) {
		this.namaKurikulum = namaKurikulum;
	}

	@Column(name = "inisialKurikulum", nullable = false, length = 15)
	public String getInisialKurikulum() {
		return this.inisialKurikulum;
	}

	public void setInisialKurikulum(String inisialKurikulum) {
		this.inisialKurikulum = inisialKurikulum;
	}

	@Column(name = "sksTa1", nullable = false)
	public int getSksTa1() {
		return this.sksTa1;
	}

	public void setSksTa1(int sksTa1) {
		this.sksTa1 = sksTa1;
	}

	@Column(name = "sksTa2", nullable = false)
	public int getSksTa2() {
		return this.sksTa2;
	}

	public void setSksTa2(int sksTa2) {
		this.sksTa2 = sksTa2;
	}

	@Column(name = "jumlahTa", nullable = false)
	public int getJumlahTa() {
		return this.jumlahTa;
	}

	public void setJumlahTa(int jumlahTa) {
		this.jumlahTa = jumlahTa;
	}

	@Column(name = "aktif", nullable = false)
	public int getAktif() {
		return this.aktif;
	}

	public void setAktif(int aktif) {
		this.aktif = aktif;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "masterKurikulum")
	public Set<MasterMahasiswa> getMasterMahasiswas() {
		return this.masterMahasiswas;
	}

	public void setMasterMahasiswas(Set<MasterMahasiswa> masterMahasiswas) {
		this.masterMahasiswas = masterMahasiswas;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "masterKurikulum")
	public Set<MasterMataKuliah> getMasterMataKuliahs() {
		return this.masterMataKuliahs;
	}

	public void setMasterMataKuliahs(Set<MasterMataKuliah> masterMataKuliahs) {
		this.masterMataKuliahs = masterMataKuliahs;
	}

}
