package id.ac.unpas.ta.david.dao;

import java.util.List;

import id.ac.unpas.ta.david.pojo.MasterPeriodeSemester;
import id.ac.unpas.ta.david.pojo.MasterTahunAkademik;




public interface MasterPeriodeSemesterDao {
	public boolean saveMasterPeriodeSemester(MasterPeriodeSemester masterPeriodeSemester);
	public boolean updateMasterPeriodeSemester(MasterPeriodeSemester masterPeriodeSemester);
	public boolean deleteMasterPeriodeSemester(MasterPeriodeSemester masterPeriodeSemester);
	
	public MasterPeriodeSemester getCurrentMasterPeriodeSemester();
	
	public List<MasterPeriodeSemester> getListMasterPeriodeSemester();
	public List<MasterPeriodeSemester> getListMasterPeriodeSemester(MasterTahunAkademik masterTahunAkademik);
	
	public List<MasterPeriodeSemester> getListMasterPeriodeSemesterBetwen(int minSemester, int maxSemester);
	
	public List<MasterPeriodeSemester> getListMasterPeriodeSemesterBetwenDesc(int minSemester, int maxSemester);
}
