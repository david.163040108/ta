package id.ac.unpas.ta.david.dao;

import java.util.List;

import id.ac.unpas.ta.david.pojo.MasterFakultas;
import id.ac.unpas.ta.david.pojo.MasterPerguruanTinggi;



public interface MasterFakultasDao {
	public boolean saveMasterFakultas(MasterFakultas masterFakultas);
	public boolean updateMasterFakultas(MasterFakultas masterFakultas);
	public boolean deleteMasterFakultas(MasterFakultas masterFakultas);
	
	public MasterFakultas getMasterFakultas(String kodeFakultas);
	
	public List<MasterFakultas> getListMasterFakultas();
	public List<MasterFakultas> getListMasterFakultas(MasterPerguruanTinggi masterPerguruanTinggi);
}
