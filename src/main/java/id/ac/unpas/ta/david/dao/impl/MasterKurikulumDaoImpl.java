package id.ac.unpas.ta.david.dao.impl;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import id.ac.unpas.ta.david.dao.MasterKurikulumDao;
import id.ac.unpas.ta.david.pojo.MasterFakultas;
import id.ac.unpas.ta.david.pojo.MasterKurikulum;
import id.ac.unpas.ta.david.pojo.MasterProgramStudi;



@Service("masterKurikulumDao")
@Transactional
@Repository
public class MasterKurikulumDaoImpl extends BaseDao<MasterKurikulum> implements MasterKurikulumDao {

	@Override
	public boolean saveMasterKurikulum(MasterKurikulum masterKurikulum) {
		// TODO Auto-generated method stub
		return save(masterKurikulum);
	}

	@Override
	public boolean updateMasterKurikulum(MasterKurikulum masterKurikulum) {
		// TODO Auto-generated method stub
		return update(masterKurikulum);
	}

	@Override
	public boolean deleteMasterKurikulum(MasterKurikulum masterKurikulum) {
		// TODO Auto-generated method stub
		return delete(masterKurikulum);
	}

	@Override
	public MasterKurikulum getMasterKurikulum(int kodeMasterKurikulum) {
		// TODO Auto-generated method stub
		Criteria criteria = createCriteria(MasterKurikulum.class);
		Criteria criteriaProdi = criteria.createCriteria("masterProgramStudi");
		criteriaProdi.createCriteria("masterFakultas");

		criteria.add(Restrictions.eq("na", "N"));
		criteria.add(Restrictions.eq("kodeMasterKurikulum", kodeMasterKurikulum));
		criteria.addOrder(Order.asc("masterProgramStudi"));
		criteria.addOrder(Order.asc("tahunKurikulum"));

		return (MasterKurikulum) criteria.uniqueResult();
	}

	public MasterKurikulum getMasterKurikulumAktif(MasterProgramStudi masterProgramStudi) {
		// TODO Auto-generated method stub
		Criteria criteria = createCriteria(MasterKurikulum.class);
		Criteria criteriaProdi = criteria.createCriteria("masterProgramStudi");
		criteriaProdi.createCriteria("masterFakultas");
		
		criteria.add(Restrictions.eq("na", "N"));
		criteria.add(Restrictions.eq("masterProgramStudi", masterProgramStudi));
		criteria.add(Restrictions.eq("aktif", 1));

		criteria.addOrder(Order.asc("masterProgramStudi"));
		criteria.addOrder(Order.asc("tahunKurikulum"));

		return (MasterKurikulum) criteria.uniqueResult();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<MasterKurikulum> getListMasterKurikulum() {
		// TODO Auto-generated method stub
		Criteria criteria = createCriteria(MasterKurikulum.class);
		Criteria criteriaProdi = criteria.createCriteria("masterProgramStudi");
		criteriaProdi.createCriteria("masterFakultas");
		
//		criteria.add(Restrictions.eq("na", "N"));
		criteria.addOrder(Order.asc("masterProgramStudi"));
		criteria.addOrder(Order.asc("tahunKurikulum"));

		return criteria.list();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<MasterKurikulum> getListMasterKurikulum(MasterFakultas masterFakultas) {
		// TODO Auto-generated method stub
		Criteria criteria = createCriteria(MasterKurikulum.class);
		Criteria criteriaProdi = criteria.createCriteria("masterProgramStudi");
		criteriaProdi.createCriteria("masterFakultas");

		criteria.add(Restrictions.eq("na", "N"));
		criteriaProdi.add(Restrictions.eq("masterFakultas", masterFakultas));

		criteria.addOrder(Order.asc("masterProgramStudi"));
		criteria.addOrder(Order.asc("tahunKurikulum"));

		return criteria.list();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<MasterKurikulum> getListMasterKurikulum(MasterProgramStudi masterProgramStudi) {
		Criteria criteria = createCriteria(MasterKurikulum.class);
		criteria.createCriteria("masterProgramStudi");
		criteria.add(Restrictions.eq("na", "N"));
		criteria.add(Restrictions.eq("masterProgramStudi", masterProgramStudi));
		criteria.addOrder(Order.desc("kodeMasterKurikulum"));
		return criteria.list();
	}

}
