package id.ac.unpas.ta.david.bean;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import id.ac.unpas.ta.david.dao.MasterFakultasDao;
import id.ac.unpas.ta.david.dao.MasterKurikulumDao;
import id.ac.unpas.ta.david.dao.MasterMahasiswaDao;
import id.ac.unpas.ta.david.dao.MasterPeriodeSemesterDao;
import id.ac.unpas.ta.david.dao.MasterProgramStudiDao;
import id.ac.unpas.ta.david.pojo.MasterFakultas;
import id.ac.unpas.ta.david.pojo.MasterKurikulum;
import id.ac.unpas.ta.david.pojo.MasterMahasiswa;
import id.ac.unpas.ta.david.pojo.MasterPerguruanTinggi;
import id.ac.unpas.ta.david.pojo.MasterPeriodeSemester;
import id.ac.unpas.ta.david.pojo.MasterProgramStudi;

@ManagedBean(name = "mahasiswaBean")
@SessionScoped
@Component(value = "mahasiswaBean")
@Scope("session")
public class MahasiswaBean extends BaseBean {

	private MasterMahasiswa mhs;
	private MasterFakultas fakultas;
	private MasterPeriodeSemester masterPeriodeSemester;
	private MasterKurikulum masterKurikulum;
	private MasterProgramStudi masterProgramStudi;

	private List<MasterMahasiswa> mahasiswas;

	private List<MasterMahasiswa> m;

	private List<MasterPeriodeSemester> periodeSemesters;

	private List<MasterProgramStudi> masterProgramStudis;

	private List<MasterKurikulum> masterKurikulums;

	@Autowired
	private MasterMahasiswaDao masterMahasiswaDao;

	@Autowired
	private MasterFakultasDao fakultasDao;
	@Autowired
	private MasterPeriodeSemesterDao masterPeriodeSemesterDao;
	@Autowired
	private MasterProgramStudiDao masterProgramStudiDao;
	@Autowired
	private MasterKurikulumDao masterKurikulumDao;

	private String periode;

	public String getPeriode() {
		return periode;
	}

	public void setPeriode(String periode) {
		this.periode = periode;
	}

	public MasterProgramStudi getMasterProgramStudi() {
		return masterProgramStudi;
	}

	public void setMasterProgramStudi(MasterProgramStudi masterProgramStudi) {
		this.masterProgramStudi = masterProgramStudi;
	}

	public MasterPeriodeSemester getMasterPeriodeSemester() {
		return masterPeriodeSemester;
	}

	public void setMasterPeriodeSemester(MasterPeriodeSemester masterPeriodeSemester) {
		this.masterPeriodeSemester = masterPeriodeSemester;
	}

	public MasterKurikulum getMasterKurikulum() {
		return masterKurikulum;
	}

	public void setMasterKurikulum(MasterKurikulum masterKurikulum) {
		this.masterKurikulum = masterKurikulum;
	}

	private MasterPerguruanTinggi perguruanTinggi;

	public MasterPerguruanTinggi getPerguruanTinggi() {
		return perguruanTinggi;
	}

	public void setPerguruanTinggi(MasterPerguruanTinggi perguruanTinggi) {
		this.perguruanTinggi = perguruanTinggi;
	}

	public List<MasterMahasiswa> getM() {
		return m;
	}

	public void setM(List<MasterMahasiswa> m) {
		this.m = m;
	}

	public List<MasterPeriodeSemester> getPeriodeSemesters() {
		return periodeSemesters;
	}

	public void setPeriodeSemesters(List<MasterPeriodeSemester> periodeSemesters) {
		this.periodeSemesters = periodeSemesters;
	}

	public List<MasterProgramStudi> getMasterProgramStudis() {
		return masterProgramStudis;
	}

	public void setMasterProgramStudis(List<MasterProgramStudi> masterProgramStudis) {
		this.masterProgramStudis = masterProgramStudis;
	}

	public List<MasterKurikulum> getMasterKurikulums() {
		return masterKurikulums;
	}

	public void setMasterKurikulums(List<MasterKurikulum> masterKurikulums) {
		this.masterKurikulums = masterKurikulums;
	}

	public MasterMahasiswaDao getMasterMahasiswaDao() {
		return masterMahasiswaDao;
	}

	public void setMasterMahasiswaDao(MasterMahasiswaDao masterMahasiswaDao) {
		this.masterMahasiswaDao = masterMahasiswaDao;
	}

	public MasterMahasiswa getMhs() {
		return mhs;
	}

	public void setMhs(MasterMahasiswa mhs) {
		this.mhs = mhs;
	}

	public List<MasterMahasiswa> getMahasiswas() {
		return mahasiswas;
	}

	public void setMahasiswas(List<MasterMahasiswa> mahasiswas) {
		this.mahasiswas = mahasiswas;
	}

	public MasterFakultas getFakultas() {
		return fakultas;
	}

	public void setFakultas(MasterFakultas fakultas) {
		this.fakultas = fakultas;
	}

	@PostConstruct
	public void init() {
		mhs = new MasterMahasiswa();
		fakultas = new MasterFakultas();
		perguruanTinggi = new MasterPerguruanTinggi();
		masterKurikulum = new MasterKurikulum();
		masterPeriodeSemester = new MasterPeriodeSemester();
		masterProgramStudi = new MasterProgramStudi();

		mahasiswas = new ArrayList<MasterMahasiswa>();
		m = new ArrayList<MasterMahasiswa>();
		masterKurikulums = new ArrayList<MasterKurikulum>();
		masterProgramStudis = new ArrayList<MasterProgramStudi>();
		periodeSemesters = new ArrayList<MasterPeriodeSemester>();
		list();
		listkurikulum();
		listPeriode();
		listProgramStudi();
		periode();

	}

	public void save() {
		mhs.setMasterKurikulum(masterKurikulum);
		mhs.setMasterPeriodeSemester(masterPeriodeSemester);
		mhs.setMasterProgramStudi(masterProgramStudi);
		if (masterMahasiswaDao.saveMasterMahasiswa(mhs)) {
			reset();
			list();
			messageInfo("Berhasil Disimpan");
		} else {
			messageError("Gagal Disimpan");
		}
	}

	public void periode() {
		m = masterMahasiswaDao.getListPeriode(masterPeriodeSemester);
		if (m != null) {
			System.out.println("Ada");
		} else {
			System.out.println("gagal");
		}
	}

	public void reset() {
		mhs = new MasterMahasiswa();
		masterKurikulum = new MasterKurikulum();
		masterProgramStudi = new MasterProgramStudi();
		masterPeriodeSemester = new MasterPeriodeSemester();
	}

	public void save2() {
		fakultas.setMasterPerguruanTinggi(perguruanTinggi);
		if (fakultasDao.saveMasterFakultas(fakultas)) {
			System.out.println("Berhasil");
		} else {
			System.out.println("Gagal");
		}
	}

	public void list() {
		mahasiswas = masterMahasiswaDao.getListMasterMahasiswa();
	}

	public void listkurikulum() {
		masterKurikulums = masterKurikulumDao.getListMasterKurikulum();
	}

	public void listProgramStudi() {
		masterProgramStudis = masterProgramStudiDao.getListMasterProgramStudi();
	}

	public void listPeriode() {
		periodeSemesters = masterPeriodeSemesterDao.getListMasterPeriodeSemester();
	}

}
