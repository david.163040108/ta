package id.ac.unpas.ta.david.dao.impl;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import id.ac.unpas.ta.david.dao.MasterTahunAkademikDao;
import id.ac.unpas.ta.david.pojo.MasterTahunAkademik;



@Service("masterTahunAkademikDao")
@Transactional
@Repository
public class MasterTahunAkademikDaoImpl extends BaseDao<MasterTahunAkademik> implements MasterTahunAkademikDao {

	@Override
	public boolean saveMasterTahunAkademik(MasterTahunAkademik masterTahunAkademik) {
		// TODO Auto-generated method stub
		return save(masterTahunAkademik);
	}

	@Override
	public boolean updateMasterTahunAkademik(MasterTahunAkademik masterTahunAkademik) {
		// TODO Auto-generated method stub
		return update(masterTahunAkademik);
	}

	@Override
	public boolean deleteMasterTahunAkademik(MasterTahunAkademik masterTahunAkademik) {
		// TODO Auto-generated method stub
		return delete(masterTahunAkademik);
	}

	@Override
	public MasterTahunAkademik getMasterTahunAkademik(int idTahunAkademik) {
		// TODO Auto-generated method stub
		Criteria criteria = createCriteria(MasterTahunAkademik.class);
		criteria.add(Restrictions.eq("idTahunAkademik", idTahunAkademik));
		criteria.addOrder(Order.asc("idTahunAkademik"));
		return (MasterTahunAkademik) criteria.uniqueResult();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<MasterTahunAkademik> getListMasterTahunAkademik() {
		// TODO Auto-generated method stub
		Criteria criteria = createCriteria(MasterTahunAkademik.class);
		criteria.addOrder(Order.desc("idTahunAkademik"));
		return (List<MasterTahunAkademik>) criteria.list();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<MasterTahunAkademik> getListMasterTahunAkademik(int minTahunAkademik, int maxTahunAkademik) {
		// TODO Auto-generated method stub
		Criteria criteria = createCriteria(MasterTahunAkademik.class);
		criteria.add(Restrictions.between("idTahunAkademik", minTahunAkademik, maxTahunAkademik));
		criteria.addOrder(Order.asc("idTahunAkademik"));
		return criteria.list();
	}

}
