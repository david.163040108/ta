package id.ac.unpas.ta.david.bean.utilities;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

import org.primefaces.event.FileUploadEvent;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;


public class UploadDownload {
	
	public String upload(FileUploadEvent event, String pathFile, int maxSize) {
		//String fileName : nama file plus extensi. contoh daftar_mahasiswa.pdf
		//String pathFile : path termasuk nama file dan extensi. contoh : C:/folder1/folder2/daftar_mahasiswa.pdf
		
		String statusUpload = "upload gagal";
		if (event != null) {

			File fileUpload = new File(pathFile);

			try {
				FileOutputStream fileOutputStream = new FileOutputStream(fileUpload);
				byte[] buffer = new byte[maxSize];

				int bulk;
				InputStream inputStream = event.getFile().getInputstream();

				while (true) {
					bulk = inputStream.read(buffer);
					if (bulk < 0) {
						break;
					}
					fileOutputStream.write(buffer, 0, bulk);

				}
				fileOutputStream.close();
				inputStream.close();
				statusUpload = "sukses";

			} catch (IOException e) {
				statusUpload = "IO Error";
				e.printStackTrace();
			} finally {

			}

		} else {
			statusUpload = "FILE NULL";
		}
		
		return statusUpload;
	}
	
	public StreamedContent download(String fileName, String extensionFile, String pathFile) {
		//String fileName : nama file plus extensi. contoh daftar_mahasiswa.pdf
		//String extensionFile : tidak menggunakan titik sebelum nama ekstensi. Contoh pdf
		//String pathFile : path termasuk nama file dan extensi. contoh : C:/folder1/folder2/daftar_mahasiswa.pdf
		
		
		System.out.println("pathDireFileDownload : " + pathFile);

		File initialFile = new File(pathFile);

		//StreamedContent fileDownLoadStream = null;
		
		try {
			InputStream inputSreamNew = new FileInputStream(initialFile);
			if (inputSreamNew != null) {
				StreamedContent fileDownLoadStream = new DefaultStreamedContent(inputSreamNew, "application/"+extensionFile, fileName);
				if (fileDownLoadStream != null) {
					// inputSreamNew.close();
					return fileDownLoadStream;
				}
				//inputSreamNew.close();
			}
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
	
	
}

