package id.ac.unpas.ta.david.pojo;

public class UserRole {
	private int idUserRole;
	private String namaUserRole;
	
	public UserRole() {
		idUserRole = 1004;
		namaUserRole = "PRODI";
	}
	
	public int getIdUserRole() {
		return idUserRole;
	}
	public void setIdUserRole(int idUserRole) {
		this.idUserRole = idUserRole;
	}
	public String getNamaUserRole() {
		return namaUserRole;
	}
	public void setNamaUserRole(String namaUserRole) {
		this.namaUserRole = namaUserRole;
	}
}
