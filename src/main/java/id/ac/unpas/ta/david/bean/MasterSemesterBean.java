package id.ac.unpas.ta.david.bean;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import id.ac.unpas.ta.david.dao.MasterSemesterDao;
import id.ac.unpas.ta.david.pojo.MasterSemester;

@ManagedBean(name = "masterSemesterBean")
@SessionScoped
@Component(value = "masterSemesterBean")
@Scope("session")
public class MasterSemesterBean {

	private int idSemester;
	private String namaSemester;

	private MasterSemester semester;

//	@Autowired
//	@Qualifier(value = "masterSemesterService")
//	private MasterSemesterService masterSemesterService;
	@Autowired
	private MasterSemesterDao masterSemesterDao;

	private List<MasterSemester> masterSemesters;

	public MasterSemester getSemester() {
		return semester;
	}

	public void setSemester(MasterSemester semester) {
		this.semester = semester;
	}

	public List<MasterSemester> getMasterSemesters() {
		return masterSemesters;
	}

	public void setMasterSemesters(List<MasterSemester> masterSemesters) {
		this.masterSemesters = masterSemesters;
	}

	public int getIdSemester() {
		return idSemester;
	}

	public void setIdSemester(int idSemester) {
		this.idSemester = idSemester;
	}

	public String getNamaSemester() {
		return namaSemester;
	}

	public void setNamaSemester(String namaSemester) {
		this.namaSemester = namaSemester;
	}

	@PostConstruct
	public void init() {
		masterSemesters = new ArrayList<MasterSemester>();
		list();
		semester = new MasterSemester();
	}

	public void list() {
		masterSemesters = masterSemesterDao.getListMasterSemesterWhere(4);
	}
	
	public void reset() {
		semester = new MasterSemester();
	}

	public void save() {
//		MasterSemester masterSemester = new MasterSemester();
//		masterSemester.setIdSemester(getIdSemester());
//		masterSemester.setNamaSemester(getNamaSemester());

		//save untuk object yang berelasi 
		// pojo tahun akademik
		// pojo master periode semester
		// masterperiodesemester.setMasterTahunAkademik 
		
		if (masterSemesterDao.saveMasterSemester(semester)) {
			reset();
			list();
			System.out.println("Berhasil");
		} else {
			System.out.println("Gagal");
		}
	}

	public void edit(MasterSemester smt) {
		semester = smt;
	}

}
