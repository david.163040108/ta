package id.ac.unpas.ta.david.dao.impl;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import id.ac.unpas.ta.david.dao.MasterPeriodeSemesterDao;
import id.ac.unpas.ta.david.pojo.MasterPeriodeSemester;
import id.ac.unpas.ta.david.pojo.MasterTahunAkademik;


@Service("masterPeriodeSemesterDao")
@Transactional
@Repository
public class MasterPeriodeSemesterDaoImpl extends BaseDao<MasterPeriodeSemester> implements MasterPeriodeSemesterDao {

	@Override
	public boolean saveMasterPeriodeSemester(MasterPeriodeSemester masterPeriodeSemester) {
		// TODO Auto-generated method stub
		return save(masterPeriodeSemester);
	}

	@Override
	public boolean updateMasterPeriodeSemester(MasterPeriodeSemester masterPeriodeSemester) {
		// TODO Auto-generated method stub
		return update(masterPeriodeSemester);
	}

	@Override
	public boolean deleteMasterPeriodeSemester(MasterPeriodeSemester masterPeriodeSemester) {
		// TODO Auto-generated method stub
		return delete(masterPeriodeSemester);
	}

	@Override
	public MasterPeriodeSemester getCurrentMasterPeriodeSemester() {
		// TODO Auto-generated method stub
		Criteria criteria = createCriteria(MasterPeriodeSemester.class);
		criteria.createCriteria("masterTahunAkademik");
		criteria.createCriteria("masterSemester");
		criteria.add(Restrictions.eq("current", true));
		System.out.println("diproses");
		return (MasterPeriodeSemester) criteria.uniqueResult();
	}	
	
	@SuppressWarnings("unchecked")
	@Override
	public List<MasterPeriodeSemester> getListMasterPeriodeSemester() {
		// TODO Auto-generated method stub
		Criteria criteria = createCriteria(MasterPeriodeSemester.class);
		criteria.createCriteria("masterTahunAkademik");
		criteria.createCriteria("masterSemester");
		criteria.addOrder(Order.desc("masterTahunAkademik"));
		criteria.addOrder(Order.desc("masterSemester"));
		
		return criteria.list();
	}

	@Override
	public List<MasterPeriodeSemester> getListMasterPeriodeSemester(MasterTahunAkademik masterTahunAkademik) {
		// TODO Auto-generated method stub
		return null;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<MasterPeriodeSemester> getListMasterPeriodeSemesterBetwen(int minSemester, int maxSemester) {
		// TODO Auto-generated method stub
		Criteria criteria = createCriteria(MasterPeriodeSemester.class);
		criteria.createCriteria("masterTahunAkademik");
		criteria.createCriteria("masterSemester");
		criteria.add(Restrictions.between("idPeriodeSemester", minSemester, maxSemester));
//		criteria.addOrder(Order.desc("masterTahunAkademik"));
//		criteria.addOrder(Order.desc("masterSemester"));
		criteria.addOrder(Order.asc("idPeriodeSemester"));
		
		return criteria.list();
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<MasterPeriodeSemester> getListMasterPeriodeSemesterBetwenDesc(int minSemester, int maxSemester) {
		// TODO Auto-generated method stub
		Criteria criteria = createCriteria(MasterPeriodeSemester.class);
		criteria.createCriteria("masterTahunAkademik");
		criteria.createCriteria("masterSemester");
		criteria.add(Restrictions.between("idPeriodeSemester", minSemester, maxSemester));
//		criteria.addOrder(Order.desc("masterTahunAkademik"));
//		criteria.addOrder(Order.desc("masterSemester"));
		criteria.addOrder(Order.desc("idPeriodeSemester"));
		
		return criteria.list();
	}



}
