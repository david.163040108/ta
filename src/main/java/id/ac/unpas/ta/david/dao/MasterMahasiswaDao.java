package id.ac.unpas.ta.david.dao;

import java.util.List;

import id.ac.unpas.ta.david.pojo.MasterFakultas;
import id.ac.unpas.ta.david.pojo.MasterMahasiswa;
import id.ac.unpas.ta.david.pojo.MasterPeriodeSemester;
import id.ac.unpas.ta.david.pojo.MasterProgramStudi;
import id.ac.unpas.ta.david.pojo.MasterTahunAkademik;

public interface MasterMahasiswaDao {
	public boolean saveMasterMahasiswa(MasterMahasiswa masterMahasiswa);

	public boolean updateMasterMahasiswa(MasterMahasiswa masterMahasiswa);

	public boolean deleteMasterMahasiswa(MasterMahasiswa masterMahasiswa);

	public boolean saveOrUpdateMasterMahasiswa(MasterMahasiswa masterMahasiswa);

	public MasterMahasiswa getMasterMahasiswa(String npm);

	public List<MasterMahasiswa> getListPeriode(MasterPeriodeSemester masterPeriodeSemester);

	public List<MasterMahasiswa> getListMasterMahasiswa();

	public List<MasterMahasiswa> getListMasterMahasiswa(MasterFakultas masterFakultas);

	public List<MasterMahasiswa> getListMasterMahasiswa(MasterProgramStudi masterProgramStudi);

	public List<MasterMahasiswa> getListMasterMahasiswa(MasterProgramStudi masterProgramStudi,
			MasterTahunAkademik masterTahunAkademik);

	public int executeQueryUpdateMasterMahasiswa(String keyFieldName, String keyValue, String updateFieldName,
			String updateValue);

}
