package id.ac.unpas.ta.david.bean;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import id.ac.unpas.ta.david.dao.UserDao;
import id.ac.unpas.ta.david.pojo.User;



@ManagedBean(name = "loginBean")
@SessionScoped
@Component(value = "loginBean")
@Scope("session")
public class LoginBean extends BaseBean {
	private String userName;
	private String password;

	@Autowired
	private UserDao userDao;
	
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	
	public String doLogin() {
		User user = userDao.getUser(userName, password);
		if (user != null) {
			setAtributProdi(user);
			System.out.println("login sukses");
			return "dikjar";
		}
		
		messageWarn("User tidak dikenali");
		return "";

	}
	
	public String doLogout() {
		HttpSession session = getRequest().getSession();
		session.removeAttribute("prodiSession");
		if (session != null) {
			session.invalidate();
			System.out.println("logout sukses");
		}
		return "logout";
	}



	private void setAtributProdi(User user) {
		// AkademikSemester akdemikSemesterAktif =
		// akademikSemesterService.getAkademikSemesterAktifProdi(user.getKodeprodi());
		HttpServletRequest request = getRequest();
		HttpSession session = request.getSession();


		UserSession userSession = new UserSession();
		userSession.setCurrentUser(user);
		userSession.setIdCurrentPeriodeSemester(20182); // hardcode
		userSession.setIdCurrentSemesterAkademik(1); //hardcode
		userSession.setIdCurrentTahunAkademik(2);
		userSession.setIdCurrentKurikulum(2016);

		session.setAttribute("prodiSession", userSession);
	}
}
