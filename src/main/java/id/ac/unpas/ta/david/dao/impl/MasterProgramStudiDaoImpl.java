package id.ac.unpas.ta.david.dao.impl;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import id.ac.unpas.ta.david.dao.MasterProgramStudiDao;
import id.ac.unpas.ta.david.pojo.MasterFakultas;
import id.ac.unpas.ta.david.pojo.MasterProgramStudi;


@Service("masterProgramStudiDao")
@Transactional
@Repository
public class MasterProgramStudiDaoImpl extends BaseDao<MasterProgramStudi> implements MasterProgramStudiDao {

	@Override
	public boolean saveMasterProgramStudi(MasterProgramStudi masterProgramStudi) {
		// TODO Auto-generated method stub
		return save(masterProgramStudi);
	}

	@Override
	public boolean updateMasterProgramStudi(MasterProgramStudi masterProgramStudi) {
		// TODO Auto-generated method stub
		return update(masterProgramStudi);
	}

	@Override
	public boolean deleteMasterProgramStudi(MasterProgramStudi masterProgramStudi) {
		// TODO Auto-generated method stub
		return delete(masterProgramStudi);
	}

	@Override
	public MasterProgramStudi getMasterProgramStudi(String kodeProgramStudi) {
		// TODO Auto-generated method stub
		Criteria criteria = createCriteria(MasterProgramStudi.class);
		Criteria criteriaFak = criteria.createCriteria("masterFakultas");
		criteriaFak.createCriteria("masterPerguruanTinggi");
		
		criteria.add(Restrictions.eq("kodeProgramStudi", kodeProgramStudi));
		
		return (MasterProgramStudi) criteria.uniqueResult();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<MasterProgramStudi> getListMasterProgramStudi() {
		// TODO Auto-generated method stub
		Criteria criteria = createCriteria(MasterProgramStudi.class);
		Criteria criteriaFak = criteria.createCriteria("masterFakultas");
		criteriaFak.createCriteria("masterPerguruanTinggi");
		criteria.addOrder(Order.asc("idProgramStudi"));

		return criteria.list();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<MasterProgramStudi> getListMasterProgramStudi(MasterFakultas masterFakultas) {
		// TODO Auto-generated method stub
		Criteria criteria = createCriteria(MasterProgramStudi.class);
		Criteria criteriaFak = criteria.createCriteria("masterFakultas");
		criteriaFak.createCriteria("masterPerguruanTinggi");
		criteria.add(Restrictions.eq("masterFakultas", masterFakultas));
		
		criteria.addOrder(Order.asc("idProgramStudi"));
		
		return criteria.list();
	}

}
