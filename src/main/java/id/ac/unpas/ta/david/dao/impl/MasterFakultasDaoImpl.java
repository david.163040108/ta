package id.ac.unpas.ta.david.dao.impl;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import id.ac.unpas.ta.david.dao.MasterFakultasDao;
import id.ac.unpas.ta.david.pojo.MasterFakultas;
import id.ac.unpas.ta.david.pojo.MasterPerguruanTinggi;


@Service("masterFakultasDao")
@Transactional
@Repository
public class MasterFakultasDaoImpl extends BaseDao<MasterFakultas> implements MasterFakultasDao {

	@Override
	public boolean saveMasterFakultas(MasterFakultas masterFakultas) {
		// TODO Auto-generated method stub
		return save(masterFakultas);
	}

	@Override
	public boolean updateMasterFakultas(MasterFakultas masterFakultas) {
		// TODO Auto-generated method stub
		return update(masterFakultas);
	}

	@Override
	public boolean deleteMasterFakultas(MasterFakultas masterFakultas) {
		// TODO Auto-generated method stub
		return delete(masterFakultas);
	}

	@Override
	public MasterFakultas getMasterFakultas(String kodeFakultas) {
		// TODO Auto-generated method stub
		Criteria criteria = createCriteria(MasterFakultas.class);
		criteria.createCriteria("masterPerguruanTinggi");
		
		criteria.add(Restrictions.eq("kodeFakultas", kodeFakultas));
		
		return (MasterFakultas) criteria.uniqueResult();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<MasterFakultas> getListMasterFakultas() {
		// TODO Auto-generated method stub
		Criteria criteria = createCriteria(MasterFakultas.class);
		criteria.createCriteria("masterPerguruanTinggi");
		
		criteria.addOrder(Order.asc("kodeFakultas"));

		return criteria.list();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<MasterFakultas> getListMasterFakultas(MasterPerguruanTinggi masterPerguruanTinggi) {
		// TODO Auto-generated method stub
		Criteria criteria = createCriteria(MasterFakultas.class);
		criteria.createCriteria("masterPerguruanTinggi");
		
		criteria.add(Restrictions.eq("masterPerguruanTinggi", masterPerguruanTinggi));
		
		criteria.addOrder(Order.asc("kodeFakultas"));

		return criteria.list();
	}

}
