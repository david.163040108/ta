package id.ac.unpas.ta.david.bean;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import id.ac.unpas.ta.david.dao.MasterFakultasDao;
import id.ac.unpas.ta.david.pojo.MasterFakultas;

@ManagedBean(name = "fakultas")
@SessionScoped
@Component(value = "fakultas")
@Scope("session")
public class FakultasBean {

	@Autowired
	private MasterFakultasDao masterFakultasDao;
	
	private List<MasterFakultas> masterFakultas;

	public List<MasterFakultas> getMasterFakultas() {
		return masterFakultas;
	}

	public void setMasterFakultas(List<MasterFakultas> masterFakultas) {
		this.masterFakultas = masterFakultas;
	}
	
	@PostConstruct
	public void init() {
		tampil();
	}
	
	public void tampil() {
		masterFakultas = masterFakultasDao.getListMasterFakultas();
	}
}
