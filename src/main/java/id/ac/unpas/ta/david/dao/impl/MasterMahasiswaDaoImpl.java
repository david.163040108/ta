package id.ac.unpas.ta.david.dao.impl;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import id.ac.unpas.ta.david.dao.MasterMahasiswaDao;
import id.ac.unpas.ta.david.pojo.MasterFakultas;
import id.ac.unpas.ta.david.pojo.MasterMahasiswa;
import id.ac.unpas.ta.david.pojo.MasterPeriodeSemester;
import id.ac.unpas.ta.david.pojo.MasterProgramStudi;
import id.ac.unpas.ta.david.pojo.MasterTahunAkademik;

@Service("masterMahasiswaDao")
@Transactional
@SuppressWarnings("deprecation")
@Repository
public class MasterMahasiswaDaoImpl extends BaseDao<MasterMahasiswa> implements MasterMahasiswaDao {

	@Override
	public boolean saveMasterMahasiswa(MasterMahasiswa masterMahasiswa) {
		// TODO Auto-generated method stub
		return save(masterMahasiswa);
	}

	@Override
	public boolean updateMasterMahasiswa(MasterMahasiswa masterMahasiswa) {
		// TODO Auto-generated method stub
		return update(masterMahasiswa);
	}

	@Override
	public boolean deleteMasterMahasiswa(MasterMahasiswa masterMahasiswa) {
		// TODO Auto-generated method stub
		return delete(masterMahasiswa);
	}

	@Override
	public boolean saveOrUpdateMasterMahasiswa(MasterMahasiswa masterMahasiswa) {
		return saveOrUpdate(masterMahasiswa);
	}

	@Override
	public MasterMahasiswa getMasterMahasiswa(String npm) {
		// TODO Auto-generated method stub
		Criteria criteria = createCriteria(MasterMahasiswa.class);
		Criteria criteriaPS = criteria.createCriteria("masterProgramStudi");
		criteriaPS.createCriteria("masterFakultas");

		// minggu depan sudah bisa input yang dibawah
		Criteria criteriaPeriodeSemester = criteria.createCriteria("masterPeriodeSemester");
		criteriaPeriodeSemester.createCriteria("masterTahunAkademik");
		criteriaPeriodeSemester.createCriteria("masterSemester");

		Criteria criteriaKurikulum = criteria.createCriteria("masterKurikulum");
		Criteria criteriaProdiKurikulum = criteriaKurikulum.createCriteria("masterProgramStudi");
		Criteria criteriaFak = criteriaProdiKurikulum.createCriteria("masterFakultas");
		criteriaFak.createCriteria("masterPerguruanTinggi");

//		criteria.add(Restrictions.like("masterPeriodeSemester", pojo dari masterPeriodeSemester))
//		SELECT * FROM master_mahasiswa JOIN master_periode_semester ON master_mahasiswa.idperiode = master_periode_semester.idperiode
//		WHERE master_periode_semester.idPeriode = masterPeriodeSemester.idPeriode
//		criteriaPeriodeSemester.add(Restrictions.eq("masterTahunAkademik", pojo dari masterTahunAKademik))
		criteria.add(Restrictions.eq("npm", npm));
//		criteria.add(Restrictions.eq("na", "N"));
		criteria.add(Restrictions.eq("aktif", 1));

		return (MasterMahasiswa) criteria.uniqueResult();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<MasterMahasiswa> getListMasterMahasiswa() {
		// TODO Auto-generated method stub
		Criteria criteria = createCriteria(MasterMahasiswa.class);
		Criteria criteriaPS = criteria.createCriteria("masterProgramStudi");
		criteriaPS.createCriteria("masterFakultas");

		Criteria criteriaPeriodeSemester = criteria.createCriteria("masterPeriodeSemester");
		criteriaPeriodeSemester.createCriteria("masterTahunAkademik");
		criteriaPeriodeSemester.createCriteria("masterSemester");

		Criteria criteriaKurikulum = criteria.createCriteria("masterKurikulum");
		Criteria criteriaProdiKurikulum = criteriaKurikulum.createCriteria("masterProgramStudi");
		Criteria criteriaFak = criteriaProdiKurikulum.createCriteria("masterFakultas");
		criteriaFak.createCriteria("masterPerguruanTinggi");

//		criteria.add(Restrictions.eq("na", "N"));

		criteria.addOrder(Order.asc("npm"));

		return criteria.list();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<MasterMahasiswa> getListMasterMahasiswa(MasterFakultas masterFakultas) {
		// TODO Auto-generated method stub
		Criteria criteria = createCriteria(MasterMahasiswa.class);
		Criteria criteriaPS = criteria.createCriteria("masterProgramStudi");
		criteriaPS.createCriteria("masterFakultas");

		Criteria criteriaPeriodeSemester = criteria.createCriteria("masterPeriodeSemester");
		criteriaPeriodeSemester.createCriteria("masterTahunAkademik");
		criteriaPeriodeSemester.createCriteria("masterSemester");

		Criteria criteriaKurikulum = criteria.createCriteria("masterKurikulum");
		Criteria criteriaProdiKurikulum = criteriaKurikulum.createCriteria("masterProgramStudi");
		Criteria criteriaFak = criteriaProdiKurikulum.createCriteria("masterFakultas");
		criteriaFak.createCriteria("masterPerguruanTinggi");

		criteria.add(Restrictions.eq("na", "N"));
		criteriaPS.add(Restrictions.eq("masterFakultas", masterFakultas));

		criteria.addOrder(Order.asc("npm"));

		return criteria.list();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<MasterMahasiswa> getListMasterMahasiswa(MasterProgramStudi masterProgramStudi) {
		// TODO Auto-generated method stub
		Criteria criteria = createCriteria(MasterMahasiswa.class);
		Criteria criteriaPS = criteria.createCriteria("masterProgramStudi");
		criteriaPS.createCriteria("masterFakultas");

		Criteria criteriaPeriodeSemester = criteria.createCriteria("masterPeriodeSemester");
		criteriaPeriodeSemester.createCriteria("masterTahunAkademik");
		criteriaPeriodeSemester.createCriteria("masterSemester");

		Criteria criteriaKurikulum = criteria.createCriteria("masterKurikulum");
		Criteria criteriaProdiKurikulum = criteriaKurikulum.createCriteria("masterProgramStudi");
		Criteria criteriaFak = criteriaProdiKurikulum.createCriteria("masterFakultas");
		criteriaFak.createCriteria("masterPerguruanTinggi");

		criteria.add(Restrictions.eq("masterProgramStudi", masterProgramStudi));
		criteria.add(Restrictions.eq("na", "N"));

		criteria.addOrder(Order.asc("npm"));

		return criteria.list();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<MasterMahasiswa> getListMasterMahasiswa(MasterProgramStudi masterProgramStudi,
			MasterTahunAkademik masterTahunAkademik) {
		// TODO Auto-generated method stub
		Criteria criteria = createCriteria(MasterMahasiswa.class);
		Criteria criteriaPS = criteria.createCriteria("masterProgramStudi");
		criteriaPS.createCriteria("masterFakultas");

		Criteria criteriaPeriodeSemester = criteria.createCriteria("masterPeriodeSemester");
		criteriaPeriodeSemester.createCriteria("masterTahunAkademik");
		criteriaPeriodeSemester.createCriteria("masterSemester");

		Criteria criteriaKurikulum = criteria.createCriteria("masterKurikulum");
		Criteria criteriaProdiKurikulum = criteriaKurikulum.createCriteria("masterProgramStudi");
		Criteria criteriaFak = criteriaProdiKurikulum.createCriteria("masterFakultas");
		criteriaFak.createCriteria("masterPerguruanTinggi");

		criteria.add(Restrictions.eq("masterProgramStudi", masterProgramStudi));
		criteria.add(Restrictions.eq("na", "N"));
		criteriaPeriodeSemester.add(Restrictions.eq("masterTahunAkademik", masterTahunAkademik));
		criteria.addOrder(Order.asc("npm"));
		return criteria.list();
	}

	@SuppressWarnings({ "rawtypes" })
	@Override
	public int executeQueryUpdateMasterMahasiswa(String keyFieldName, String keyValue, String updateFieldName,
			String updateValue) {
		String q = "UPDATE MasterMahasiswa as mhs SET mhs." + updateFieldName + " = :" + updateValue + " WHERE mhs."
				+ keyFieldName + " = :" + keyValue;
		System.out.println(q);
		Query query = createQuery(q);
		query.setParameter(keyFieldName, keyValue);
		query.setParameter(updateFieldName, updateValue);

		return executeQueryUpdate(query);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<MasterMahasiswa> getListPeriode(MasterPeriodeSemester masterPeriodeSemester) {
		// TODO Auto-generated method stub
		Criteria criteria = createCriteria(MasterMahasiswa.class);
		Criteria criteriaPS = criteria.createCriteria("masterProgramStudi");
		criteriaPS.createCriteria("masterFakultas");

		Criteria criteriaPeriodeSemester = criteria.createCriteria("masterPeriodeSemester");
		criteriaPeriodeSemester.createCriteria("masterTahunAkademik");
		criteriaPeriodeSemester.createCriteria("masterSemester");

		Criteria criteriaKurikulum = criteria.createCriteria("masterKurikulum");
		Criteria criteriaProdiKurikulum = criteriaKurikulum.createCriteria("masterProgramStudi");
		Criteria criteriaFak = criteriaProdiKurikulum.createCriteria("masterFakultas");
		criteriaFak.createCriteria("masterPerguruanTinggi");

		criteria.add(Restrictions.eq("masterPeriodeSemester", masterPeriodeSemester));
		criteria.addOrder(Order.asc("npm"));

		return criteria.list();
	}
}
