package id.ac.unpas.ta.david.bean;

import id.ac.unpas.ta.david.pojo.User;

public class UserSession {
	private User currentUser;
	private int idCurrentPeriodeSemester;
	private int idCurrentSemesterAkademik;
	private int idCurrentTahunAkademik;
	private int idCurrentKurikulum;
	
	public User getCurrentUser() {
		return currentUser;
	}
	public void setCurrentUser(User currentUser) {
		this.currentUser = currentUser;
	}
	public int getIdCurrentPeriodeSemester() {
		return idCurrentPeriodeSemester;
	}
	public void setIdCurrentPeriodeSemester(int idCurrentPeriodeSemester) {
		this.idCurrentPeriodeSemester = idCurrentPeriodeSemester;
	}
	public int getIdCurrentSemesterAkademik() {
		return idCurrentSemesterAkademik;
	}
	public void setIdCurrentSemesterAkademik(int idCurrentSemesterAkademik) {
		this.idCurrentSemesterAkademik = idCurrentSemesterAkademik;
	}
	public int getIdCurrentTahunAkademik() {
		return idCurrentTahunAkademik;
	}
	public void setIdCurrentTahunAkademik(int idCurrentTahunAkademik) {
		this.idCurrentTahunAkademik = idCurrentTahunAkademik;
	}
	public int getIdCurrentKurikulum() {
		return idCurrentKurikulum;
	}
	public void setIdCurrentKurikulum(int idCurrentKurikulum) {
		this.idCurrentKurikulum = idCurrentKurikulum;
	}

	
	
}
