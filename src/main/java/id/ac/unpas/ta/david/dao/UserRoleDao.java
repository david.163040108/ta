package id.ac.unpas.ta.david.dao;

import id.ac.unpas.ta.david.pojo.UserRole;

public interface UserRoleDao {
	public UserRole getUserRole(String namaRole);	
}
