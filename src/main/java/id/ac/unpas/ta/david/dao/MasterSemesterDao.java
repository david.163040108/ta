package id.ac.unpas.ta.david.dao;

import java.util.List;

import id.ac.unpas.ta.david.pojo.MasterSemester;

public interface MasterSemesterDao {
	public boolean saveMasterSemester(MasterSemester masterSemester);

	public boolean updateMasterSemester(MasterSemester masterSemester);

	public boolean deleteMasterSemester(MasterSemester masterSemester);
	
	public List<MasterSemester> getListMasterSemesterWhere(int id);

	public List<MasterSemester> getListMasterSemester();
}
