package id.ac.unpas.ta.david.pojo;

public class User {
	private int idUser;
	private UserRole userRole;
	private String userName;
	private String password;
	private int aktif;
	
	
	public User() {
		idUser = 1004304;
		userRole = new UserRole();
		userName = "informatika";
		password = "informatika";
		aktif = 1;
		
	}
	
	
	public int getIdUser() {
		return idUser;
	}
	public void setIdUser(int idUser) {
		this.idUser = idUser;
	}
	public UserRole getUserRole() {
		return userRole;
	}
	public void setUserRole(UserRole userRole) {
		this.userRole = userRole;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public int getAktif() {
		return aktif;
	}
	public void setAktif(int aktif) {
		this.aktif = aktif;
	}
	
	
	
	
}
