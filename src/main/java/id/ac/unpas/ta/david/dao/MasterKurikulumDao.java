package id.ac.unpas.ta.david.dao;

import java.util.List;

import id.ac.unpas.ta.david.pojo.MasterFakultas;
import id.ac.unpas.ta.david.pojo.MasterKurikulum;
import id.ac.unpas.ta.david.pojo.MasterProgramStudi;





public interface MasterKurikulumDao {
	public boolean saveMasterKurikulum(MasterKurikulum masterKurikulum);
	public boolean updateMasterKurikulum(MasterKurikulum masterKurikulum);
	public boolean deleteMasterKurikulum(MasterKurikulum masterKurikulum);
	
	public MasterKurikulum getMasterKurikulum(int kodeMasterKurikulum);
	public MasterKurikulum getMasterKurikulumAktif(MasterProgramStudi masterProgramStudi);
	
	public List<MasterKurikulum> getListMasterKurikulum();
	public List<MasterKurikulum> getListMasterKurikulum(MasterFakultas masterFakultas);
	public List<MasterKurikulum> getListMasterKurikulum(MasterProgramStudi masterProgramStudi);
	
	
}
