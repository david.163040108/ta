package id.ac.unpas.ta.david.dao;

import java.util.List;

import id.ac.unpas.ta.david.pojo.MasterPerguruanTinggi;




public interface MasterPerguruanTinggiDao {
	public boolean saveMasterPerguruanTinggi(MasterPerguruanTinggi masterPerguruanTinggi);
	public boolean updateMasterPerguruanTinggi(MasterPerguruanTinggi masterPerguruanTinggi);
	public boolean deleteMasterPerguruanTinggi(MasterPerguruanTinggi masterPerguruanTinggi);
	
	public MasterPerguruanTinggi getMasterPerguruanTinggi(String kodePerguruanTinggi);
	
	public List<MasterPerguruanTinggi> getListMasterPerguruanTinggi();
}
