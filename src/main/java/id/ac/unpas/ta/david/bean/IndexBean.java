package id.ac.unpas.ta.david.bean;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import id.ac.unpas.ta.david.dao.MasterMahasiswaDao;
import id.ac.unpas.ta.david.pojo.MasterMahasiswa;


@ManagedBean(name = "indexBean")
@SessionScoped
@Component(value = "indexBean")
@Scope("session")
public class IndexBean {
	
	private String npm;
	
	@Autowired
	private MasterMahasiswaDao masterMahasiswaDao;
	

	public void setNpm(String npm) {
		System.out.println("npm "+npm);
		this.npm = npm;
	}
	

	public String getNpm() {
		return npm;
	}

	@PostConstruct
	public void init() {
		System.out.println("Init");
	}
	
	
	public void loadMahasiswa() {
		MasterMahasiswa masterMahasiswa = masterMahasiswaDao.getMasterMahasiswa(npm);
		if(masterMahasiswa != null) {
			System.out.println(masterMahasiswa.getNpm());
			System.out.println(masterMahasiswa.getNamaMahasiswa());
		}
	}
	
}
