package id.ac.unpas.ta.david.dao.impl;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import id.ac.unpas.ta.david.dao.MasterSemesterDao;
import id.ac.unpas.ta.david.pojo.MasterSemester;

@Service("masterSemesterDao")
@Transactional
@Repository
public class MasterSemesterDaoImpl extends BaseDao<MasterSemester> implements MasterSemesterDao {

	@Override
	public boolean saveMasterSemester(MasterSemester masterSemester) {
		// TODO Auto-generated method stub
		return save(masterSemester);
	}

	@Override
	public boolean updateMasterSemester(MasterSemester masterSemester) {
		// TODO Auto-generated method stub
		return update(masterSemester);
	}

	@Override
	public boolean deleteMasterSemester(MasterSemester masterSemester) {
		// TODO Auto-generated method stub
		return delete(masterSemester);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<MasterSemester> getListMasterSemester() {
		Criteria criteria = createCriteria(MasterSemester.class);
		criteria.addOrder(Order.asc("idSemester"));
		return criteria.list();
	}

	@Override
	public List<MasterSemester> getListMasterSemesterWhere(int id) {
		// TODO Auto-generated method stub
		Criteria criteria = createCriteria(MasterSemester.class);
		criteria.add(Restrictions.gt("idSemester", id));
		criteria.addOrder(Order.asc("idSemester"));
		
		return criteria.list();
	}

}
