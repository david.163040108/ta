package id.ac.unpas.ta.david.bean;

import javax.faces.application.FacesMessage;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;


public class BaseBean {
		
	protected FacesContext fc() {
		return FacesContext.getCurrentInstance();
	}
	
	protected ExternalContext externalContext() {
		return fc().getExternalContext();
	}
	
	protected HttpServletRequest servletRequest() {
		return (HttpServletRequest) externalContext().getRequest();
	}
	
	protected void messageInfo(String message) {
        fc().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Info", message));
    }
 
	protected void messageWarn(String message) {
        fc().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, "Warning!", message));
    }
 
	protected void messageError(String message) {
        fc().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error!", message));
    }
 
	protected void messageFatal(String message) {
        fc().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, "Fatal!", message));
    }
	
	
	private FacesContext getFacesContext() {
		return FacesContext.getCurrentInstance();
	}
	
	private ExternalContext getExternalContext() {
		return getFacesContext().getExternalContext();
	}
	
	public HttpServletRequest getRequest() {
		return (HttpServletRequest) getExternalContext().getRequest();
	}

	
	protected UserSession getUserSession() {
		return (UserSession) getRequest().getSession().getAttribute("prodiSession");
	}

	
}
