package id.ac.unpas.ta.david.dao;

import id.ac.unpas.ta.david.pojo.User;

public interface UserDao {
	public User getUser(String username, String password);	
}
