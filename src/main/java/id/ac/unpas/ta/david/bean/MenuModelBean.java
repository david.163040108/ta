package id.ac.unpas.ta.david.bean;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

import org.primefaces.model.menu.DefaultMenuItem;
import org.primefaces.model.menu.DefaultMenuModel;
import org.primefaces.model.menu.DefaultSubMenu;
import org.primefaces.model.menu.MenuModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import id.ac.unpas.ta.david.constant.Constant;




@ManagedBean(name = "menuModelBean")
@Component(value = "menuModelBean")
@SessionScoped
@Scope("session")
public class MenuModelBean extends BaseBean {


	private MenuModel menuModel;

	public MenuModel getMenuModel() {
		return menuModel;
	}

	@PostConstruct
	public void init() {
		menuModel = new DefaultMenuModel();
		userMenuModel(getUserSession().getCurrentUser().getUserRole().getIdUserRole());
	}

	private void userMenuModel(int idUserRole) {
		switch (idUserRole) {
		case Constant.USER_ROLE_ID_PRODI:
			dikjarMenuModel();
			break;
		default:
			break;
		}
	}

	private void dikjarMenuModel() {
		DefaultMenuItem beranda = new DefaultMenuItem();
		beranda.setUrl("/faces/prodi/index.xhtml");
		beranda.setValue("Beranda");
		beranda.setIcon("fa fa-home");

		DefaultMenuItem inputMahasiswaTaItem = new DefaultMenuItem();
		inputMahasiswaTaItem.setUrl("/faces/prodi/fakultas.xhtml");
		inputMahasiswaTaItem.setValue("Beranda");
		inputMahasiswaTaItem.setIcon("fa fa-pencil-square-o");

		DefaultSubMenu userProfilSubMenu = new DefaultSubMenu();
		userProfilSubMenu.setLabel(getUserSession().getCurrentUser().getUserName());
		userProfilSubMenu.setStyleClass("ui-menubar-options");
		
		DefaultMenuItem userProfileLogout = new DefaultMenuItem();
		userProfileLogout.setValue("logout");
		userProfileLogout.setAjax(false);
		userProfileLogout.setCommand("#{LoginBean.doLogout()}");
		userProfilSubMenu.addElement(userProfileLogout);

		menuModel.addElement(beranda);
		menuModel.addElement(inputMahasiswaTaItem);
		menuModel.addElement(userProfilSubMenu);

	}







}
