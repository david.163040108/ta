package id.ac.unpas.ta.david.dao;

import java.util.List;

import id.ac.unpas.ta.david.pojo.MasterTahunAkademik;




public interface MasterTahunAkademikDao {
	public boolean saveMasterTahunAkademik(MasterTahunAkademik masterTahunAkademik);
	public boolean updateMasterTahunAkademik(MasterTahunAkademik masterTahunAkademik);
	public boolean deleteMasterTahunAkademik(MasterTahunAkademik masterTahunAkademik);
	
	public MasterTahunAkademik getMasterTahunAkademik(int idTahunAkademik);
	
	public List<MasterTahunAkademik> getListMasterTahunAkademik();
	public List<MasterTahunAkademik> getListMasterTahunAkademik(int minTahunAkademik, int maxTahunAkademik);
}
