package id.ac.unpas.ta.david.dao.impl;

import java.util.List;
import java.util.TimeZone;

import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;

@SuppressWarnings("deprecation")
public class BaseDao<T> {
	@Autowired
	private SessionFactory sessionFactory;	
	
	
	//nambahan getTimeZone di SessionFactory
	public SessionFactory getSessionFactory() {
		sessionFactory.withOptions().jdbcTimeZone(TimeZone.getTimeZone("Asia/Jakarta"));
		return sessionFactory;
	}

	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}
	
	public boolean save(T entity){ 
		try {
			getSessionFactory().getCurrentSession().save(entity);
			return true;
		} catch (HibernateException e) {
			e.printStackTrace();
		}
		
		return false;
	}
	
	public boolean saveOrUpdate(T entity){
		try {
			getSessionFactory().getCurrentSession().saveOrUpdate(entity);
			return true;
		} catch (HibernateException e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		
		return false;
	}
	
	public boolean update(T entity){
		try {
			getSessionFactory().getCurrentSession().update(entity);
			return true;
		} catch (HibernateException e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		
		return false;
	}
	
	public boolean delete(T entity){
		try {
			getSessionFactory().getCurrentSession().delete(entity);
			return true;
		} catch (HibernateException e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		
		return false;
	}
	
	
	
	public Criteria createCriteria(Class<T> persistent){
		return getSessionFactory().getCurrentSession().createCriteria(persistent);
	}
	
	
	
	
	@SuppressWarnings("rawtypes")
	public List listCriterias(Criteria criteria){
		return criteria.list();
	}
	

	@SuppressWarnings({ "rawtypes" })
	public Query createQuery(String query){
		return getSessionFactory().getCurrentSession().getSession().createQuery(query);
	}
	
	@SuppressWarnings({ "rawtypes" })
	public int executeQueryUpdate(Query query) {
		return query.executeUpdate();
	}
	
	@SuppressWarnings({ "rawtypes" })
	public List executeQueryList(String query) {
		return getSessionFactory().getCurrentSession().getSession().createQuery(query).list();
	}
}
